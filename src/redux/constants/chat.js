export const INIT_CHAT = 'INIT_CHAT';
export const SEND_MSG = 'SEND_MSG';
export const UPDATE_MSG = 'UPDATE_MSG';
export const GET_MSG = 'GET_MSG';
