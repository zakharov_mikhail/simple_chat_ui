import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import generalReducer from "./reducers/general";
import chatReducer from "./reducers/chat";
import notificationsReducer from "./reducers/notifications";


const reducer = combineReducers({
    generalReducer, chatReducer, notificationsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    reducer,
    composeEnhancers(
        applyMiddleware(thunk)
    )
);

export default store;
