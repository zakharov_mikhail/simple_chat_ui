import {INIT_CHAT, SEND_MSG, UPDATE_MSG, GET_MSG} from "../constants/chat";


export const initChat = list => {
    return dispatch => {
        dispatch({
            type: INIT_CHAT,
            payload: list
        })
    }
};

export const sendMsg = msg => {
    return dispatch => {
        dispatch({
            type: SEND_MSG,
            payload: msg
        })
    }
};

export const updateMsg = id => {
    return dispatch => {
        dispatch({
            type: UPDATE_MSG,
            payload: id
        })
    }
};

export const getMsg = msg => {
    return dispatch => {
        dispatch({
            type: GET_MSG,
            payload: msg
        })
    }
};