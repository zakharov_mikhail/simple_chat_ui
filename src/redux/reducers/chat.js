import {INIT_CHAT, SEND_MSG, UPDATE_MSG, GET_MSG} from "../constants/chat";


const initState = [];

export default (state = initState, action) => {
    switch(action.type) {
        case INIT_CHAT :
            console.log("init_chat", action.payload);
            return [...action.payload];
        case SEND_MSG :
            let msg = {...action.payload};
            msg.fetching = true;
            console.log("send_msg", msg);
            return [...state, msg];
        case UPDATE_MSG :
            console.log("update_msg", action.payload);
            let newList = state.map(msg => {
               if(msg.key === action.payload){
                   delete msg.fetching;
               }
               return msg;
            });
            return [...newList];
        case GET_MSG :
            msg = action.payload;
            console.log("get_msg", msg);
            return [...state, msg];
    default :
        return state
    }
}