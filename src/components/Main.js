import React, {Component} from "react";
// import { Provider } from "react-redux";

import Chat from "./chat"


export default class Main extends Component {

    render(){
        return(
            <div className="main">
                <h1>Simple Chat</h1>
                <div className="main-chat">
                    <Chat />
                </div>
            </div>
        )
    }
}


