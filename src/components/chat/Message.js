import React, {Component} from "react"
import PropTypes from "prop-types"
//
// export default class Message extends Component {
//
//     render(){
//         return(
//             <div className="chat-message">
//                 <div className="chat-message-name">Name</div>
//                 <div className="chat-message-body">Text</div>
//             </div>
//         )
//     }
// }
const Message = ({name, text, fetching}) => {

    return(
        <div className={"chat-message" + (fetching ? " fetching" : "")} >
            <div className="chat-message-name">{name}</div>
            <div className="chat-message-body">{text}</div>
        </div>
    )
};

Message.propTypes = {
    name: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
};

export default Message;