import React, {Component, createRef} from "react";
import {connect} from "react-redux";

import Message from "./Message"

import {initChat, sendMsg, getMsg, updateMsg} from "../../redux/actions/chat";


class Chat extends Component {

    state = {
        name: "",
        text: "",
        fetchingMsgs: {},
        lastConnectTime: null
    };

    node = createRef();

    scrollToBottomNode = () => {
        let node = this.node.current;
        node.scrollTo(0, node.scrollHeight)
    };

    componentDidMount(){
        this.initSocket();
    }

    componentDidUpdate(prevProps) {
        if (this.props.chatReducer.length !== prevProps.chatReducer.length) {
            this.scrollToBottomNode();
        }
    }

    componentWillUnmount() {
        this.ws.close();
    }

    initSocket = () => {
        this.ws = new WebSocket('ws://localhost:8081');
        this.ws.onopen = evt => {
            console.log("WS_open", evt);
        };
        this.ws.onclose = evt => {
            console.log("WS_close", evt);
            this.initSocket();
        };
        this.ws.onmessage = evt => {
            this.handleGotMsg(JSON.parse(evt.data));
        };
        this.ws.onerror = evt => {
            console.log("WS_error", evt);
        };
    };

    changeName = (event) => {
        let name = event.target.value;
        this.setState({name});
    };

    changeText = event => {
        let text = event.target.value;
        this.setState({text});
    };

    submit = () => {
        let {name, text} = this.state;
        if(!name || !text){
            return alert("All the fields should be filled!");
        }
        console.log(name, text);
        let msg = {
          key: +new Date(),
          name,
          text
        };
        this.props.sendMsg(msg);
        this.ws.send(JSON.stringify({type: "msg", data: msg}));
        this.setState(prevState => ({text: "", lastConnectTime: +new Date(), fetchingMsgs: {...prevState, [msg.key]: true}}));
    };

    handleGotMsg = msg => {
        console.log("handleGotMsg ws ", msg);
        let {data, type} = msg ;
        switch (type) {
            case "init":
                this.props.initChat(data);
                break;
            case "msg":
                this.props.getMsg(data);
                break;
            case "update_msg":
                this.props.updateMsg(data);
                break;
            default:
                break;
        }

        this.setState({lastConnectTime: +new Date()});
    };

    handleKeyUp = (event) => {
        if (event.keyCode == 13){
            return this.submit();
        }
    };

    render(){

        return(
            <div className="chat">
                <div className="chat-body" ref={this.node} >
                    {this.props.chatReducer && this.props.chatReducer.length &&
                    this.props.chatReducer.map(item => <Message key={item.key} {...item} />) || <div>No messages</div>}
                </div>
                <div className="chat-footer">
                    <input type="text" className="chat-footer-name" placeholder="Nickname" value={this.state.name} onChange={this.changeName} />
                    <input type="text" className="chat-footer-msg" placeholder="Message" value={this.state.text} onChange={this.changeText} onKeyUp={this.handleKeyUp} />
                    <button className="chat-footer-submit" onClick={this.submit}>Send</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        chatReducer: state.chatReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        initChat: list => dispatch(initChat(list)),
        sendMsg: msg => dispatch(sendMsg(msg)),
        updateMsg: key => dispatch(updateMsg(key)),
        getMsg: msg => dispatch(getMsg(msg))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat)